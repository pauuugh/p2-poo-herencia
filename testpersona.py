from persona import Paciente, Medico
import unittest

class TestPersona(unittest.TestCase):
    def test_ver_historial(self):
        paciente = Paciente('Enrique', 'Conde', '17-04-2002' , '632762-A', 'Fractura de cadera')
        historial = paciente.ver_historial()
        self.assertEqual(historial, 'El historial del paciente es Fractura de cadera')

    def test_consultar_agenda(self):
        medico=Medico('Ivan', 'González', '20-11-2006', '46765-C', 'Traumatología','2024-02-09 12:00, 2024-03-04 14:00, 2024-03-04 15:00')
        agenda = medico.consultar_agenda()
        self.assertEqual(agenda,'Sus próximas citas programadas son : 2024-02-09 12:00, 2024-03-04 14:00, 2024-03-04 15:00 para la especialidad de Traumatología')

if __name__ == '__main__':
    unittest.main()

