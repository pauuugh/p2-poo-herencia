class Persona:
    def __init__(self, nombre, apellidos, fecha,DNI):
        self.nombre= nombre
        self.apellidos = apellidos
        self.fecha = fecha
        self.DNI = DNI

    def setnombre(self, nombre):
        self.nombre = nombre
    def getnombre(self):
        return self.nombre
    def setapellidos(self, apellidos):
        self.apellidos = apellidos
    def getapellidos(self):
        return self.apellidos
    def setfecha(self, fecha):
        self.fecha = fecha
    def getfecha(self):
        return self.fecha
    def setDNI(self, DNI):
        self.DNI = DNI
    def getDNI(self):
        return self.DNI

    def __str__(self):
        return 'La persona llamada {nombre}, {apellidos} nacido el {fecha} con DNI: {DNI}'.format(nombre= self.nombre, apellidos= self.apellidos, fecha=self.fecha, DNI = self.DNI)

class Paciente(Persona):
    def __init__(self,nombre, apellidos, fecha,DNI, historial):
        super().__init__(nombre, apellidos, fecha,DNI)   
        self.historial = historial

    def ver_historial(self):
        return 'El historial del paciente es {historial}'.format(historial = self.historial)

class Medico(Persona):
    def __init__(self, nombre, apellidos, fecha,DNI, especialidad, citas):
        super().__init__(nombre, apellidos, fecha,DNI) 
        self.especialidad = especialidad
        self.citas = citas

    def consultar_agenda(self):
        return ('Sus próximas citas programadas son : {citas} para la especialidad de {especialidad}'.format(citas = self.citas, especialidad=self.especialidad))
    
persona=Persona('Paula', 'González', '08-09-2003', '7328-A')
paciente=Paciente('Enrique', 'Conde', '17-04-2002' , '632762-A', 'Fractura de cadera')
medico=Medico('Ivan', 'González', '20-11-2006', '46765-C', 'Traumatología', '2024-02-09 12:00, 2024-03-04 14:00, 2024-03-04 15:00')

personas = [persona, paciente, medico]
for individuo in personas:
    print (individuo)
    if individuo==paciente:
        print(individuo.ver_historial())
    elif individuo==medico:
        print(individuo.consultar_agenda())
